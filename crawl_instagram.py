# -*- coding: utf-8

# crawl_instagram.py
# Instagram 크롤링 스크립트
# by Wooyeol Kim (wooyekim@gmail.com)

from selenium import webdriver
import urllib
import requests
import sys
import os
import json
import pprint
import argparse

class InstagramQuery(object):
  def __init__(self, target, max_nodes = None):
    self.target = target
    self.max_nodes = max_nodes

  @staticmethod
  def getIG():
    raise NotImplementedError('getIG is not defined')

  @staticmethod
  def getRef():
    raise NotImplementedError('getRef is not defined')

  @staticmethod
  def __getMediaFromFirstPageScript__():
    raise NotImplementedError('getMediaFromFirstPageScript is not defined')

  # Get the first media JSON instance from the first page
  @classmethod
  def getMediaFromFirstPage(self, driver):
    script = self.__getMediaFromFirstPageScript__()
    return driver.execute_script(script)

  @staticmethod
  def getNodesPerPage():
    return 12

  @staticmethod
  def __getIDScript__():
    raise NotImplementedError('getIDScript is not defined')

  # Get user/location ID from the current session
  def getID(self, driver):
    script = self.__getIDScript__()
    return driver.execute_script(script)

  def getURL(self):
    raise NotImplementedError('getURL is not defined')

class InstagramUserQuery(InstagramQuery):
  @staticmethod
  def getIG():
    return "user"

  @staticmethod
  def getRef():
    return "users"

  @staticmethod
  def __getMediaFromFirstPageScript__():
    return "return window._sharedData.entry_data.ProfilePage[0].user.media"

  @staticmethod
  def __getIDScript__():
    return "return window._sharedData.entry_data.ProfilePage[0].user.id"

  def getURL(self):
    return 'https://www.instagram.com/{user_name}/'.format(user_name = self.target)

class InstagramLocationQuery(InstagramQuery):
  @staticmethod
  def getIG():
    return "location"

  @staticmethod
  def getRef():
    return "locations"

  @staticmethod
  def __getMediaFromFirstPageScript__():
    return "return window._sharedData.entry_data.LocationsPage[0].location.media"

  @staticmethod
  def __getIDScript__():
    return "return window._sharedData.entry_data.LocationsPage[0].location.id"

  def getURL(self):
    return 'https://www.instagram.com/explore/locations/{location_id}/'.format(location_id = self.target)

class InstagramTagQuery(InstagramQuery):
  @staticmethod
  def getIG():
    return "hashtag"

  @staticmethod
  def getRef():
    return "tags"

  @staticmethod
  def __getMediaFromFirstPageScript__():
    return "return window._sharedData.entry_data.TagPage[0].tag.media"

  @staticmethod
  def getNodesPerPage():
    return 9

  def getID(self, driver):
    return self.target

  def getURL(self):
    encoded_tag_name = urllib.parse.quote(self.target.encode('utf8'))
    return 'https://www.instagram.com/explore/tags/{tag_name}/'.format(tag_name = encoded_tag_name)

######################  Prepare Selenium WebDriver #######################
def getDriver():
  return webdriver.PhantomJS("phantomjs/bin/phantomjs",
                             service_args = ["--webdriver-loglevel=ERROR"])
def killDriver(driver):
	pid = driver.service.process.pid
	os.kill(pid, signal.SIGKILL)
##########################################################################

# Get POST data which retrieves pictures from a given user ID
# target_id : ID of the target
# media_after_id : The maximum ID of the picture
# nodes_per_page : The number of pictures to retrieve
# query_id : UNKNOWN yet
def buildQuery(ig_keyword, target_id, media_after_id, ref_keyword,
               nodes_per_page = 12, query_id = None):
  q = """ig_{ig_keyword}({target_id}) {{ media.after({media_after_id}, {nodes_per_page}) {{
  count,
  nodes {{
    caption,
    code,
    comments {{
      count
    }},
    comments_disabled,
    date,
    dimensions {{
      height,
      width
    }},
    display_src,
    id,
    is_video,
    likes {{
      count
    }},
    owner {{
      id
    }},
    thumbnail_src,
    video_views
  }},
  page_info
}}
 }}""".format(ig_keyword = ig_keyword, target_id = target_id,
              media_after_id = media_after_id, nodes_per_page = nodes_per_page)
  ref = "{ref_keyword}::show".format(ref_keyword = ref_keyword)
  payload = {'q' : q,
             'ref' : ref}
  if query_id is not None:
    payload['query_id'] = str(query_id)
  return payload

# Get request session info from selenium driver
def getInstagramSession(driver):
  session = requests.session()
  cstr = ""
  for cookie in driver.get_cookies():
    session.cookies.set(cookie['name'], cookie['value'])
    cstr += cookie['name'] + "=" + cookie['value'] + ";"
    if cookie['name'] == "csrftoken":
      csrftoken = cookie['value']
  headers = {
      "accept":"*/*",
      "accept-encoding":"gzip, deflate, br",
      "accept-language":"ko-KR,ko;q=0.8,en-US;q=0.6,en;q=0.4",
      "content-type":"application/x-www-form-urlencoded",
#      "content-length":"",
      "cookie":cstr,
      "origin":"https://www.instagram.com",
      "referer":driver.current_url,
      "user-agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36",
      "x-csrftoken":csrftoken,
      "x-instagram-ajax":"1",
      "x-requested-with":"XMLHttpRequest"
  }
  session.headers.update(headers)
  return session

# Crawl media from the given (user name)/(location ID)/(tag name)
def crawlMedia(driver, query):
  nodes = []
  # Open page
  url = query.getURL()
  driver.get(url)

  # Retrieve query keywords and IDs
  target_id = query.getID(driver)
  ig_keyword = query.getIG()
  nodes_per_page = query.getNodesPerPage()
  ref_keyword = query.getRef()

  # Extract session info
  session = getInstagramSession(driver)

  # Retrieve the first media page
  media = query.getMediaFromFirstPage(driver)
  while ((media is not None) and
         (query.max_nodes is None or len(nodes) < query.max_nodes)):
    # Append nodes
    for node in media['nodes']:
      nodes.append(node)
      if (query.max_nodes is not None and len(nodes) >= query.max_nodes):
        return nodes

    # Get page info to check if there is next page to load
    page_info = media['page_info']
    if page_info['has_next_page']:
      media_after_id = page_info['end_cursor']
      payload = buildQuery(ig_keyword, target_id, media_after_id, ref_keyword,
                           nodes_per_page)
      r = session.post(url = "https://www.instagram.com/query/", data = payload)
      data = json.loads(r.content.decode())
      media = data['media']
    else:
      media = None
  return nodes

parser = argparse.ArgumentParser(description = "Crawl media from Instagram.")
parser.add_argument("type", help = "Type of the crawling target",
                    choices=['user', 'location', 'tag'])
parser.add_argument("-m", "--max", help = "Crawl (maximum) MAX nodes", type=int)
parser.add_argument("target", help = "user name, location ID or tag name")
args = parser.parse_args()
if (args.type == 'user'):
  query = InstagramUserQuery(args.target, args.max)
elif (args.type == 'location'):
  query = InstagramLocationQuery(args.target, args.max)
elif (args.type == 'tag'):
  query = InstagramTagQuery(args.target, args.max)

driver = getDriver()
nodes = crawlMedia(driver, query)
driver.quit()

pp = pprint.PrettyPrinter(indent = 2)
for node in nodes:
  pp.pprint(node)
  print()
