# -*- coding: utf-8

# wordcount.py
# 자주 등장하는 word의 빈도 count
# by Wooyeol Kim (wooyekim@gmail.com)

import os
import sys
from bs4 import BeautifulSoup as BS
from konlpy.tag import Kkma

class UnknownFormatException(Exception):
  def __init__(self, value):
    self.value = value
  def __str__(self):
    return repr(self.value)

# Given BeautifulSoup html tree, extract div tag that contains its post
def extractPostDiv(htmltree):
  # Check SE2 post view first
  divs = html_tree.select("div.se_doc_contents_start")
  if(len(divs) != 0):
    return divs[0].find_next_sibling("div")
  # Check SE3 post view
  divs = html_tree.select("div#postViewArea")
  if(len(divs) != 0):
    return divs[0]
  raise UnknownFormatException("Unknown format")

def getBS(htmldata):
  htmltree = BS(html_data, "html.parser")
  # Remove script and iframe
  [rmv.extract() for rmv in htmltree(['script', 'iframe'])]
  return htmltree

kkma = Kkma()
freqmap = {}
dirname = "blogs/"
c = 0
listdir = os.listdir(dirname)
for filename in listdir:
  c += 1
  if not filename.endswith(".html"):
    continue
  with open(dirname + filename, 'r') as f:
    html_data = f.read()
    f.close()
    print("{}/{}".format(c, len(listdir)), end='\r', file=sys.stderr)

    html_tree = getBS(html_data)
    div = extractPostDiv(html_tree)
    text = div.getText()
    words = kkma.nouns(text)
    for word in words:
      if not (word in freqmap):
        freqmap[word] = 1
      else:
        freqmap[word] = freqmap[word] + 1

for key,value in freqmap.items():
  if value == 1:
    continue
  print(key + " : " + str(value))
