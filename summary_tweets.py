# -*- coding: utf-8

import pdb
import tweepy
import jsonpickle
import datetime
import os, fnmatch
from bs4 import BeautifulSoup as BS

def find(pattern, path):
  result = []
  for root, dirs, files in os.walk(path):
    for name in files:
      if fnmatch.fnmatch(name, pattern):
        result.append(os.path.join(root, name))
  return result

query = "제주도"
target_date = datetime.date(2016, 11, 24)
prefix = query + "_" + target_date.isoformat()

outputfile = open(prefix + ".html", "w")
outputfile.write("<html><body>")
files = find(prefix + "*.tweet", "tweets/")
for filename in files:
  with open(filename, 'r') as f:
    jsontext = f.read()
  status = jsonpickle.decode(jsontext)
  outputfile.write("<p>" + status.text  + "</p><hr/>")
outputfile.write("</body></html>")
outputfile.close()
