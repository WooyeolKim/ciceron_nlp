# -*- coding: utf-8

# crawl_sjw.py
# 승정원일기 크롤링 스크립트
# by Bryan RHEE (junhang.lee@ciceron.me)

from bs4 import BeautifulSoup as bs
import urllib
import requests
import sys
import os
import xmltodict
import traceback
import argparse
import re
import psycopg2

######################        Query Making         #######################
class SJWQueryMaking(object):
    def __init__(self, conn):
        self.conn = conn

        self.empire = [
                'A' # 인조
              , 'F' # 영조
              , 'K' # 고종
              , 'L' # 순종
              ]
        self.empireIdx = -1
        self.year = 0
        self.month = 0
        self.day = 0
        self.proof_no = -1

        self.getTreeBaseURL = "http://db.itkc.or.kr/itkcdb/layout/treeMenuIframe.jsp?"
        self.getSentenceBaseURL = "http://db.itkc.or.kr/itkcdb/text/nodeViewIframe.jsp?"
        self.empireURL = []
        self.yearURL = []
        self.monthURL = []
        self.dayURL = []
        self.sentenceURL = []

        self.parsedParams = []

    def setEmpireTree(self):
        res = requests.get('http://db.itkc.or.kr/itkcdb/text/treexml/ms.xml?bizName=MS&mode=getTreeChronicle&NodeId=ms_k-A001&guboonNodeId=kojong&peopleCode=A')

        try:
            dat = xmltodict.parse(res.text)
            for item in dat['Tree']['TreeNode'][0]['TreeNode']:
                self.empireURL.append(item['@Param'])
        except:
            #print(res.text)
            traceback.print_exc()

    def setYearTree(self, url):
        res = requests.get(self.getTreeBaseURL + url)

        try:
            dat = xmltodict.parse(res.text)
            for item in dat['Tree']['TreeNode']:
                self.yearURL.append(item['@Param'])

        except:
            #print(res.text)
            traceback.print_exc()

    def setMonthTree(self, url):
        res = requests.get(self.getTreeBaseURL + url)

        try:
            dat = xmltodict.parse(res.text)
            for item in dat['Tree']['TreeNode']:
                self.monthURL.append(item['@Param'])

        except:
            #print(res.text)
            traceback.print_exc()

    def setDayTree(self, url):
        res = requests.get(self.getTreeBaseURL + url)

        try:
            dat = xmltodict.parse(res.text)
            for item in dat['Tree']['TreeNode']:
                self.dayURL.append(item['@Param'])

        except:
            #print(res.text)
            traceback.print_exc()

    def setSentenceTree(self, url):
        res = requests.get(self.getTreeBaseURL + url)

        try:
            dat = xmltodict.parse(res.text)
            for item in dat['Tree']['TreeNode']:
                self.sentenceURL.append(item['@Param'])

        except:
            #print(res.text)
            traceback.print_exc()

    def getEmpireTree(self):
        return self.empireURL

    def getYearTree(self):
        return self.yearURL

    def getMonthTree(self):
        return self.monthURL

    def getDayTree(self):
        return self.dayURL

    def getSentenceTree(self):
        return self.sentenceURL

    def loadSentenceParams(self, line):
        self.sentenceURL.append(line)

    def _parseParams(self, param):
        param_temp = param.split('&')
        dict_item = {}
        #dayId = '000'
        #chapId = 1
        for item in param_temp:
            key_value = item.split('=')
            dict_item[ key_value[0] ] = key_value[1].strip()
            #if key_value[0] == 'daId' and dayId != key_value[1]:
            #    dayId = key_value[1]
            #    chapId = 1
            #    dict_item['chapId'] = 1

            #elif key_value[0] == 'daId' and dayId == key_value[1]:
            #    chapId += 1
            #    dict_item['chapId'] = chapId

            #else:
            #    pass

        return dict_item

    def parseParams(self):
        for param in self.sentenceURL:
            parsed_param = self._parseParams(param)
            self.parsedParams.append(parsed_param)

    def _loadHTML(self, url):
        html = urllib.request.urlopen(url)
        return html

    def extractKoreanText(self, url):
        html = self._loadHTML(url)
        htmltree = bs(html, "html.parser")
        textpart = htmltree.find(id='topTextDiv').get_text()
        chapId = int(re.search(r'\[[0-9]+\]', htmltree.select_one('font.datenum').get_text() ).group(0).replace('[', '').replace(']', ''))
        return textpart, chapId

    def extractChineseText(self, url):
        html = self._loadHTML(url)
        htmltree = bs(html, "html.parser")
        textpart = htmltree.select_one('p.paragraph')
        return textpart.get_text()

    def iterator(self):
        return zip(self.sentenceURL, self.parsedParams)

    def dataInsert(self, param, chapId, kor_text, zh_text):
        query = """
            INSERT INTO SJW.RAW_CRAWL
              (param, chapId, kor, zh)
            VALUES
              (%s, %s, %s, %s)
        """
        cursor = self.conn.cursor()
        try:
            cursor.execute(query, (param, chapId, kor_text, zh_text, ))
        except:
            traceback.print_exc()
            self.conn.rollback()
            return False

        self.conn.commit()
        return True

    def errorLoger(self, param, chapId, msg):
        query = """
            INSERT INTO SJW.ERROR
              (param, chapId, msg)
            VALUES
              (%s, %s, %s)
        """
        cursor = self.conn.cursor()
        try:
            cursor.execute(query, (param, chapId, msg, ))
        except:
            traceback.print_exc()
            self.conn.rollback()
            return False

        self.conn.commit()
        return True


if __name__ == "__main__":
    baseITKCURL = "http://db.itkc.or.kr/itkcdb/text/nodeViewIframe.jsp?{params}"
    baseHistoryURL = "http://sjw.history.go.kr/id/SJW-{jwId}{moId}{daId}-{chapId:03d}00"
    parser = argparse.ArgumentParser(description='Translation agent')
    parser.add_argument('--dbpass', dest='dbpass', help='DB password')
    parser.add_argument('--urlFile', dest='urlFile', help='uflFile', default='nothing')
    args = parser.parse_args()
    
    dbpass = args.dbpass; urlFile = args.urlFile
    CONNECTION = "host=aristoteles.ciceron.xyz port=5432 dbname=SENTENCE user=ciceron_web password={}"
    conn = psycopg2.connect(CONNECTION.format(dbpass))

    queryMaker = SJWQueryMaking(conn)

    if urlFile == 'nothing':
        sys.stderr.write('Start Crawling')
        queryMaker.setEmpireTree()
        
        sys.stderr.write('Year')
        for url in queryMaker.getEmpireTree():
            queryMaker.setYearTree(url)
        
        sys.stderr.write('Month')
        for url in queryMaker.getYearTree():
            queryMaker.setMonthTree(url)
        
        sys.stderr.write('Day')
        for url in queryMaker.getMonthTree():
            queryMaker.setDayTree(url)
        
        sys.stderr.write('Sentence')
        for url in queryMaker.getDayTree():
            queryMaker.setSentenceTree(url.replace('&mode=getTreeChronicle', ''))
        
        for url in queryMaker.getSentenceTree():
            print(url)
        
        #pp.pprint(queryMaker.sentenceURL)
        
        #for param in queryMaker.nextParams():
        #    #print(param)
        #    print(baseURL.format(**param))
        
    else:
        with open(urlFile, 'r') as f:
            for line in f.readlines():
                queryMaker.loadSentenceParams(line)
    
    queryMaker.parseParams()
    for whole_param, dict_params in queryMaker.iterator():
        print( "Params: '{}'".format(dict_params.get('gaLid')) )
        if dict_params.get('gaLid') == None or dict_params.get('gaLid') == '':
            continue
    
        try:
            url_itkc = baseITKCURL.format(params=whole_param)
            print(url_itkc)
            kor_text, chapId = queryMaker.extractKoreanText(url_itkc)
            print(kor_text)
            url_history = baseHistoryURL.format(
                    jwId=dict_params['jwId']
                  , moId=dict_params['moId']
                  , daId=dict_params['daId']
                  , chapId=chapId
                  )
            print(url_history)
            zh_text = queryMaker.extractChineseText(url_history)
            print(zh_text)
    
            is_inserted = queryMaker.dataInsert(whole_param, chapId, kor_text, zh_text)
    
        except Exception as err:
            print("On exception:")
            is_logged = queryMaker.errorLoger(whole_param, chapId, str(err))
