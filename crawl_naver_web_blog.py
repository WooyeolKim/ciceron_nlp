# -*- coding: utf-8

# search_naver_web_blog.py
# 네이버 블로그 검색 테스트용 스크립트
# 네이버 검색창을 직접 이용함.
# by Wooyeol Kim (wooyekim@gmail.com)

import pdb
import urllib
from bs4 import BeautifulSoup as BS
from selenium import webdriver
import sys
from datetime import datetime, timedelta
import concurrent.futures as Futures
import os
import signal
import traceback

########################## Timeout for opening a page ########################
TIMEOUT = 10
MAX_RETRY = 1
##########################################################################

######################  Prepare Selenium WebDriver #######################
def getDriver():
  return webdriver.PhantomJS("phantomjs/bin/phantomjs",
                             service_args = ["--webdriver-loglevel=ERROR"])
def killDriver(driver):
  pid = driver.service.process.pid
  os.kill(pid, signal.SIGKILL)
driver = getDriver()
##########################################################################

# 주어진 BeautifulSoup element로부터 link의 URL을 추출
def extract_links(html_tree):
  ulist = html_tree.find("ul", attrs = {"id" : "elThumbnailResultArea",
                                        "class" : "type01"})
  arr = []
  if (ulist is None):
      return arr 
  for link in ulist.findAll("li"):
    dt = link.find("dt")
    ahref = dt.find("a")['href']
    arr.append(ahref)
  return arr

# 주어진 키워드로 네이버 블로그를 웹에서 검색하고,
# 검색결과 URL들을 리스트와, 다음 검색결과 페이지가 있는지 여부를 반환
# query : 검색어
# date : 검색하고자 하는 날짜 (YYYYMMDD)
# 블로그 시작 index
def search(query, date, start = 1):
  values = {"where" : "post",     # 블로그 포스트 검색
            "sm" : "tab_pge",     # ??
            "query" : query,
            "st" : "sim",         # 유사도 기준 정렬
            "date_option" : "8",  # 지정된 날짜 범위 옵션
            "date_from" : date,
            "date_to" : date,
            "dup_remove" : "1",   # 유사검색결과 제거
            "srchby" : "all",     # 검색범위 전체(제목+본문)
            "ie" : "utf8",
            "start" : str(start)}
  data = urllib.parse.urlencode(values)
  response = urllib.request.urlopen(search.base_url + "?" + data)
  html_tree = BS(response.read(), "html.parser")
  links = extract_links(html_tree)

  paging = html_tree.find("div", attrs = {"class" : "paging"})
  anchors = paging.findAll("a")
  last_anchor = anchors[-1]
  has_next_page = ('class' in last_anchor.attrs)\
                  and (last_anchor['class'][0] == "next")
  return (links, has_next_page)
search.base_url = "https://search.naver.com/search.naver"

class UnknownFormatException(Exception):
  def __init__(self, value):
    self.value = value
  def __str__(self):
    return repr(self.value)

def open_blogpage(ID, link):
  driver.get(link)
  html_tree = BS(driver.page_source, "html.parser")
  frame = html_tree.find("frame", attrs = {"id" : "mainFrame"})
  if frame is None:
    raise UnknownFormatException("No mainFrame in " + link)
  driver.switch_to.frame("mainFrame")
  content = driver.find_element_by_xpath("//table[@id='printPost1']")
  #TODO: blog.naver.com 과 blog.daum.net을 따로 처리 해줘야 함.
  #다음 블로그: http://blog.daum.net/bsh0729/17194789
  return content.get_attribute("innerHTML")

def crawl(query, date, start_item = 1, end_item = 10):
  item = start_item
  has_next_page = True
  while has_next_page and (item <= end_item):
    (links, has_next_page) = search(query, date, item)
    for link in links:
      print (str(item) + " Processing " + link)
      trials = 1
      while trials <= MAX_RETRY:
        timeout = False
        with Futures.ThreadPoolExecutor(max_workers = 1) as executor:
          start_time = datetime.now()
          ft = executor.submit(open_blogpage, item, link)
          try:
            content = ft.result(timeout = TIMEOUT)
            duration = datetime.now() - start_time
            fname = "{0}_{1}_{2:05d}".format(query, date, item) + ".html"
            f = open(resdir + fname, "w")
            f.write(content)
            f.close()
            print (str(duration) + " sec elapsed")
          except Futures.TimeoutError as e:
            # Kill the driver
            global driver
            driver.close()
            driver = getDriver()
            if trials == 1:
              print ("Timeout while opening " + link)
            if trials <= MAX_RETRY:
              print ("Retrying (" + str(trials) + ")")
            trials = trials + 1
            timeout = True
          except UnknownFormatException as e:
            print (str(e))
          except :
            print ("Unexpected error: " + str(sys.exc_info()[0]) + " in " + link)
            traceback.print_exc()
          finally :
            sys.stdout.flush()
        if not timeout:
          break
      item = item + 1
      if (item > end_item):
        break;

resdir = "res/"
time = datetime.strptime("20161125", "%Y%m%d")
endtime = datetime.strptime("20161214", "%Y%m%d")
while (time <= endtime):
    datestr = str(time.strftime("%Y%m%d"))
    print(datestr)
    try:
        crawl("제주도", datestr, 1, 1000)
    except:
        print("Unexpected error: " + str(sys.exc_info()[0]) + " in " + datestr)
    time = time + timedelta(days=1)
driver.close()
