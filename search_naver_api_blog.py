# -*- coding: utf-8

# search_naver_blog.py
# 네이버 블로그 검색 테스트용 스크립트
# by Wooyeol Kim (wooyekim@gmail.com)

import pdb
import urllib
import urllib2
import xml.etree.ElementTree as ET
from selenium import webdriver
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

#########################  Naver API OAuth Info  #########################
client_id = "N_ByvOEtuWaImuBq_huM"
client_secret = "jzZiDbrK__"
##########################################################################

######################  Prepare Selenium WebDriver #######################
driver = webdriver.PhantomJS("phantomjs/bin/phantomjs")
##########################################################################


# 주어진 키워드로 네이버 블로그를 검색하고, 그 결과를 XML로 반환.
# XML 형식은 https://developers.naver.com/docs/search/blog 참조.
def search(query, display = None, start = None, sort = None):
  values = {"query" : query}
  if(display is not None): values["display"] = display
  if(start is not None): values["start"] = start
  if(sort is not None): values["sort"] = sort
  data = urllib.urlencode(values)
  req = urllib2.Request(search.base_url + "?" + data)
  req.add_header("X-Naver-Client-Id", client_id)
  req.add_header("X-Naver-Client-Secret", client_secret)
  response = urllib2.urlopen(req)
  xml_string = response.read()
  xml_tree = ET.fromstring(xml_string)
  return xml_tree
search.base_url = "https://openapi.naver.com/v1/search/blog.xml"

def extract_links(xml):
  links = xml.findall("./channel/item/link")
  arr = []
  for link in links:
    arr.append(link.text)
  return arr

def open_blogpage(link):
  driver.get(link)
  driver.switch_to.frame("mainFrame")
  content = driver.find_element_by_xpath("//table[@id='printPost1']")
  #TODO: blog.naver.com 과 blog.daum.net을 따로 처리 해줘야 함.
  #다음 블로그: http://blog.daum.net/bsh0729/17194789
  return content.get_attribute("innerHTML")

resdir = "res/"

xml = search("제주도")
f = open(resdir + "xml.xml", "w")
xmlstr = ET.tostring(xml, encoding = "utf-8", method = "xml")
f.write(xmlstr)
f.close()

links = extract_links(xml)
cnt = 0
for link in links:
  f = open(resdir + "html" + str(cnt) + ".html", "w")
  html = open_blogpage(link)
  f.write(html)
  f.close()
  cnt = cnt + 1
