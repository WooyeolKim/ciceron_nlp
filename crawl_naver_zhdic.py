# -*- coding: utf-8 -*-
# Execute this with python3

# crawl_naver_zhdic.py
# 주어진 문장을 네이버 중국어 사전에 검색하고
# 결과를 저장하는 스크립트
# by Wooyeol Kim (wooyekim@gmail.com)

import urllib.request
import urllib.parse
import re
import time
from bs4 import BeautifulSoup as BS
import openpyxl # XLSX
import xlrd # XLS
import psycopg2
import traceback
import argparse
import nltk

import jieba


class DataImporter():
    def __init__(self, **kwargs):
        raise NotImplementedError()

    def readNextRecord(self):
        raise NotImplementedError()

class DataExporter(object):
    def __init__(self, **kwargs):
        raise NotImplementedError()

    def writeRecord(self, *args):
        raise NotImplementedError()

    def close(self):
        raise NotImplementedError()

class KoreanGoKrDataImporter(DataImporter):
    def __init__(self, **kwargs):
        self.__words__ = []
        self.__currIdx__ = 0
        self.__suffixRe__ = re.compile("[0-9]*$")

        if 'filename' not in kwargs:
            raise RuntimeError('No filename is given')
        workbook = xlrd.open_workbook(kwargs['filename'])
        worksheet = workbook.sheet_by_index(0)

        # 맨 처음 row 는 title
        arr = [self.__suffixRe__.sub("", cell.value)\
            for cell in worksheet.col_slice(1, 1)]
        # Remove duplicates
        arr = sorted(arr)
        prev = None
        for text in arr:
            if text == prev:
                continue
            prev = text
            self.__words__.append(prev)

    def insertNewLetter(self, letter):
        if letter not in self.__words__:
            self.__words__.append(letter)

    def readNextRecord(self):
        if self.__currIdx__ == len(self.__words__):
            return None
        text = self.__words__[self.__currIdx__]
        self.__currIdx__ += 1
        return text

class XLSDataExporter(object):
    def __init__(self, **kwargs):
        title = "NaverZhDic"
        if 'filename' not in kwargs:
            raise RuntimeError('No filename is given')
        if 'title' in kwargs:
            title = kwargs['title']
        self.__filename__ = kwargs['filename']
        self.__workbook__ = openpyxl.Workbook()
        self.__worksheet__ = self.__workbook__.create_sheet(title, 0)
        self.__currIdx__ = 0

    def writeRecord(self, *args):
        for idx in range(0, len(args)):
            cell = self.__worksheet__.cell(row = self.__currIdx__ + 1, column = idx + 1)
            cell.value = args[idx]
            self.__currIdx__ += 1

    def close(self):
        self.__workbook__.save(self.__filename__)

class DBDataExporter(DataExporter):
    def __init__(self, conn):
        self.conn = conn

    def writeRecord(self, *args):
        cursor = self.conn.cursor()
        query = """
            INSERT INTO ZH.ZH_SENTENCES
            (chinese, korean) -- Use autogen ID
            VALUES
            (%s, %s)
        """
        try:
            cursor.execute(query, (args[0], args[1]))
        except:
            self.conn.rollback()
            traceback.print_exc()
            print("Duplicated. Chinese: {} | Korean: {}".format(args[0], args[1]))
            return False

        print("Crawled. Chinese: {} | Korean: {}".format(args[0], args[1]))
        self.conn.commit()
        return True

class NaverZhDicCrawler():
    def __init__(self, importer, exporter):
        self.__re_suffix__ = re.compile("→.*")
        self.__importer__ = importer
        self.__exporter__ = exporter
        self.__tokenizer__ = nltk.data.load('tokenizers/punkt/english.pickle')
  
    def __getHTMLTree__(self, query, page):
        url = "http://cndic.naver.com/search/example"
        payload = {"q": query, "pageNo": page, "direct":"false"}
        response = urllib.request.urlopen(url + "?" + urllib.parse.urlencode(payload))
        htmltree = BS(response.read(), "html.parser")
        currpage_paginate = htmltree.select_one('div#content > div.term_result.line_b_none \
            > div.old_example_area > div > strong')
        try:
            has_next = currpage_paginate.find_next_sibling("a") is not None
        except:
            has_next = False

        return (htmltree, has_next)

    def __sentencePostProcessing(self, chinese, korean):
        chinese_array = self.__tokenizer__.tokenize(chinese)
        korean_array = self.__tokenizer__.tokenize(korean)

        # 한국어에 주석같은 쓰레기가 많이 달려있어서
        # 중국어 문장 길이에 맞춰 문장 리턴
        return list(zip(chinese_array, korean_array)) 

    def __processSinglePage__(self, htmltree):
        sentences_chinese = htmltree.select('div#content > div.term_result.line_b_none \
            > div.old_example_area > dl > dt > span.sc')
        sentences_korean = htmltree.select('div#content > div.term_result.line_b_none \
            > div.old_example_area > dl > dd')
        assert len(sentences_chinese) == len(sentences_korean)
        rslt = []
        for chinese, korean in list(zip(sentences_chinese, sentences_korean)):
            chinese = chinese.get_text().replace('。', '.').strip()
            korean = korean.get_text().strip()
            korean = self.__re_suffix__.sub("", korean)
            post_processed_rslt = self.__sentencePostProcessing(chinese, korean)
            for pure_chinese, pure_korean in post_processed_rslt:
                rslt.append((pure_chinese, pure_korean))
        return rslt
  
    def __crawlSingleQuery__(self, query):
        rslt_query = []
        page = 1
        has_next = True
        while has_next:
            (htmltree, has_next) = self.__getHTMLTree__(query, page)
            rslt_page = self.__processSinglePage__(htmltree)
            rslt_query.extend(rslt_page)
            page += 1

        time.sleep(2)
        return rslt_query

    def __splitIntoLetter(self, sentence):
        words = jieba.cut(sentence)
        return words
  
    def crawl(self):
        while True:
            text = self.__importer__.readNextRecord()
            if text is None:
                break
            result = self.__crawlSingleQuery__(text)
            for chinese, korean in result:
                #self.__exporter__.writeRecord(text, chinese, korean)
                self.__exporter__.writeRecord(chinese, korean)
                splitted_words = self.__splitIntoLetter(chinese)
                for word in splitted_words:
                    self.__importer__.insertNewLetter(word)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--dbpass', dest='dbpass', help='DB password')
    parser.add_argument('--initFile', dest='initFile', help='Initial word file')
    args = parser.parse_args()
    conn = psycopg2.connect("host=ciceron.xyz port=5432 dbname=SENTENCE user=ciceron password={}".format(args.dbpass))

    importer = KoreanGoKrDataImporter(filename=args.initFile)
    #exporter = XLSDataExporter(filename = "./rslt.xlsx")
    exporter = DBDataExporter(conn)
    crawler = NaverZhDicCrawler(importer, exporter)
    crawler.crawl()
    exporter.close()
